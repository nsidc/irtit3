'''
A script for converting raw IRTIT3 data to netcdf/hdf5 granules with geolocation metadata

Usage:
    python convert.py path/to/granule.hdr

    Outputs: path/to/granule.nc
'''
from pathlib import Path
import sys
from collections import defaultdict
import re

import rasterio.transform
import xarray
import numpy as np


GRID_MAPPING_VARIABLE_NAME = 'polar_stereographic'
X_DIM, Y_DIM = 'x', 'y'

FLOAT_REGEX = re.compile('-?\d*\.\d*')
INT_REGEX = re.compile('-?\d+\.?0*')


def grid_mapping_attrs_from_metadata(metadata, affine):
    '''
    Return dictionary of grid_mapping attributes from metadata
    '''
    origin_latitude = -90.0 if metadata['hemisphere'] == 'South' else 90.0

    attrs = {
        'GeoTransform': '{} {} {} {} {} {}'.format(*affine.to_gdal()),
        'inverse_flattening': 298.25722356300003,
        'semi_major_axis': 6378137.0,
        'false_easting': 0.0,
        'false_northing': 0.0,
        'grid_mapping_name': 'polar_stereographic',
        'latitude_of_projection_origin': origin_latitude,
        'longitude_of_prime_meridian': metadata['reference_longitude'],
        'standard_parallel': metadata['latitude_of_true_scale'],
        'straight_vertical_longitude_from_pole': 0.0,
        'spatial_ref': wkt_from_metadata(metadata, affine)
    }

    return attrs


def wkt_from_metadata(metadata, affine):
    '''
    Return WKT string from metadata
    '''
    wkt = '\n'.join([
        f'PROJCS["{metadata["projection_name"]}",',
        '    GEOGCS["WGS 84",',
        '        DATUM["WGS_1984",',
        '            SPHEROID["WGS 84",6378137,298.257223563,',
        '                AUTHORITY["EPSG","7030"]],',
        '            AUTHORITY["EPSG","6326"]],',
        '        PRIMEM["Greenwich",0,',
        '            AUTHORITY["EPSG","8901"]],',
        '        UNIT["degree",0.0174532925199433,',
        '            AUTHORITY["EPSG","9122"]],',
        '        AUTHORITY["EPSG","4326"]],',
        '    PROJECTION["Polar_Stereographic"],',
        f'    PARAMETER["latitude_of_origin",{int(metadata["latitude_of_true_scale"])}],',
        f'    PARAMETER["central_meridian",{int(metadata["reference_longitude"])}],',
        '    PARAMETER["scale_factor",1],',
        '    PARAMETER["false_easting",0],',
        '    PARAMETER["false_northing",0],',
        '    UNIT["metre",1,AUTHORITY["EPSG","9001"]],',
        '    AXIS["Easting",EAST],',
        '    AXIS["Northing",NORTH]]'
    ])

    return wkt


def init_dataset(metadata, affine):
    '''
    Initialize dataset with grid mapping and coordinate variables
    '''
    rows, cols = metadata['shape']
    x, _ = affine * (np.arange(cols), np.zeros((cols,)))
    _, y = affine * (np.zeros((rows,)), np.arange(rows))
    ds = xarray.Dataset({GRID_MAPPING_VARIABLE_NAME: None},
                        coords={Y_DIM: y, X_DIM: x})
    ds[GRID_MAPPING_VARIABLE_NAME].attrs.update(
        grid_mapping_attrs_from_metadata(metadata, affine)
    )
    ds[X_DIM].attrs['standard_name'] = 'projection_x_coordinate'
    ds[X_DIM].attrs['long_name'] = 'x coordinate of projection'
    ds[X_DIM].attrs['units'] = 'm'
    ds[Y_DIM].attrs['standard_name'] = 'projection_y_coordinate'
    ds[Y_DIM].attrs['long_name'] = 'y coordinate of projection'
    ds[Y_DIM].attrs['units'] = 'm'

    return ds


def add_grid(dataset, name, data):
    '''
    Add data grid associated with grid mapping
    '''
    dataset[name] = ((Y_DIM, X_DIM), data)
    dataset[name].attrs['grid_mapping'] = GRID_MAPPING_VARIABLE_NAME


def read_header(filepath):
    '''
    Read IRTIT3 header file into metadata dict
    '''
    with Path(filepath).open('r') as f:
        metadata = {key.strip(): value.strip()
                    for key, value
                    in [line.strip().split('=')
                        for line in f]}

    for key, value in metadata.items():
        if FLOAT_REGEX.match(value):
            metadata[key] = float(value)

    metadata['shape'] = int(metadata['nrows']), int(metadata['ncols'])

    return metadata


def read_raster(filepath, shape, dtype='float32', nodata=None):
    '''
    Read IRTIT3 raster binary into numpy array
    '''
    raster = np.fromfile(str(filepath), dtype=dtype)
    if nodata is not None:
        raster[raster == nodata] = np.nan

    raster = raster.reshape(shape)

    return raster


if __name__ == '__main__':
    header_filepath = sys.argv[1]
    metadata = defaultdict(lambda: None, read_header(header_filepath))

    dtype = np.float32
    assert metadata['data_type'] == 'float', f'Unexpected data type, {metadata["data_type"]}'

    ice_thickness_filepath = Path(header_filepath).with_suffix('.ice_thickness')
    ice_thickness = read_raster(ice_thickness_filepath,
                                shape=metadata['shape'],
                                nodata=metadata['nodata_value'])
    thickness_err_filepath = Path(header_filepath).with_suffix('.thickness_err')
    thickness_err = read_raster(ice_thickness_filepath,
                                shape=metadata['shape'],
                                nodata=metadata['nodata_value'])

    affine = rasterio.transform.from_origin(
        metadata['UL_x'],
        metadata['UL_y'],
        metadata['col_spacing'],
        metadata['row_spacing']
    )

    dataset = init_dataset(metadata, affine)
    add_grid(
        dataset=dataset,
        name='ice_thickness',
        data=ice_thickness
    )
    add_grid(
        dataset=dataset,
        name='thickness_err',
        data=thickness_err
    )

    output_filepath = Path(header_filepath).with_suffix('.nc')
    dataset.to_netcdf(output_filepath)
