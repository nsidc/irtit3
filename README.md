# irtit3

Conversion script for raw IRTIT3 tomographic data into NetCDF4 format.

## Installation

Requirements:

* python ~= 3.6
* xarray ~= 0.10
* numpy ~= 1.15

Installation with [conda](https://conda.io/miniconda.html)

```
conda env create -f environment.yml
conda activate irtit3
```

## Usage

```
python convert.py path/to/<granule>.hdr
```

This will generate a `<granule>.nc` file in `path/to`.
